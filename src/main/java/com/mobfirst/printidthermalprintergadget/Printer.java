package com.mobfirst.printidthermalprintergadget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import com.mobfirst.printidthermalprintergadget.listeners.ConnectionResult;
import com.mobfirst.printidthermalprintergadget.usb.UsbManager;

public class Printer {

    private final int INTERFACE_INDEX = 0;
    private final int ENDPOINT_INDEX = 1;

    private UsbManager usbManager;

    public Printer(final Context context) throws Exception {
        this.usbManager = new UsbManager(context, 12342, 1);
    }

    public void connect(final ConnectionResult result) {
        if(this.usbManager != null) this.usbManager.connect(INTERFACE_INDEX, ENDPOINT_INDEX, result);
    }

    public void printText(final String text) {

        if(this.usbManager != null && text != null) {
            this.usbManager.sendData(text.getBytes());
        }
    }

    public void printBitmap(final Bitmap bitmap) {
        if(this.usbManager != null && bitmap != null) {

            int bitmapWidth = bitmap.getWidth();
            int bitmapHeight = bitmap.getHeight();
            int roundedWidth = bitmapWidth + bitmapWidth % 8;

            Bitmap resizedBitmap = Utils.getResizedBitmap(bitmap, roundedWidth, bitmapHeight);
            if(resizedBitmap != null) {

                Bitmap monochromeBitmap = Utils.getMonochromeBitmap(resizedBitmap);
                if(monochromeBitmap != null) {

                    int pixelCount = roundedWidth * bitmapHeight;
                    int dataLength = pixelCount / 8;
                    int functionLength = dataLength + 9;
                    int commandLength = functionLength + 6;

                    int[] pixels = new int[pixelCount];
                    byte[] command = new byte[commandLength];

                    int commandIndex = 15;

                    command[0] = (byte)29;
                    command[1] = (byte)40;
                    command[2] = (byte)76;
                    command[3] = (byte)(functionLength & 0xFF);
                    command[4] = (byte)((functionLength >> 8) & 0xFF);
                    command[5] = (byte)48;
                    command[6] = (byte)112;
                    command[7] = (byte)48;
                    command[8] = (byte)1;
                    command[9] = (byte)1;
                    command[10] = (byte)49;
                    command[11] = (byte)(roundedWidth & 0xFF);
                    command[12] = (byte)((roundedWidth >> 8) & 0xFF);
                    command[13] = (byte)(bitmapHeight & 0xFF);
                    command[14] = (byte)((bitmapHeight >> 8) & 0xFF);

                    bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, roundedWidth, bitmapHeight);
                    for(int i = 0; i < pixelCount; i += 8) {

                        byte p7 = (byte)(((pixels[i] & 0x80) >> 7) == 0 ? 1 : 0);
                        byte p6 = (byte)(((pixels[i + 1] & 0x80) >> 7) == 0 ? 1 : 0);
                        byte p5 = (byte)(((pixels[i + 2] & 0x80) >> 7) == 0 ? 1 : 0);
                        byte p4 = (byte)(((pixels[i + 3] & 0x80) >> 7) == 0 ? 1 : 0);
                        byte p3 = (byte)(((pixels[i + 4] & 0x80) >> 7) == 0 ? 1 : 0);
                        byte p2 = (byte)(((pixels[i + 5] & 0x80) >> 7) == 0 ? 1 : 0);
                        byte p1 = (byte)(((pixels[i + 6] & 0x80) >> 7) == 0 ? 1 : 0);
                        byte p0 = (byte)(((pixels[i + 7] & 0x80) >> 7) == 0 ? 1 : 0);

                        command[commandIndex++] = (byte)((p7 << 7) | (p6 << 6) | (p5 << 5) |
                                (p4 << 4) | (p3 << 3) | (p2 << 2) | (p1 << 1) | (p0));
                    }

                    this.usbManager.sendData(command);
                    this.usbManager.sendData(new byte[] { 29, 40, 76, 2, 0, 48, 2 });
                }
            }
        }
    }

    public void printQrCode(final String text) {
        if(text != null) {

            byte[] data = text.getBytes();
            byte[] command = new byte[data.length + 8];

            command[0] = (byte)29;
            command[1] = (byte)40;
            command[2] = (byte)107;
            command[3] = (byte)((data.length + 3) & 0xFF);
            command[4] = (byte)(((data.length + 3) >> 8) & 0xFF);
            command[5] = (byte)49;
            command[6] = (byte)80;
            command[7] = (byte)48;

            System.arraycopy(data, 0, command, 8, data.length);

            this.usbManager.sendData(new byte[] { 29, 40, 107, 4, 0, 49, 65, 50, 0 });
            this.usbManager.sendData(new byte[] { 29, 40, 107, 3, 0, 49, 67, 10 });
            this.usbManager.sendData(new byte[] { 29, 40, 107, 3, 0, 49, 69, 50 });
            this.usbManager.sendData(command);
            this.usbManager.sendData(new byte[] { 29, 40, 107, 3, 0, 49, 81, 48 });
        }
    }

    public void cut() {
        if(this.usbManager != null) this.usbManager.sendData(new byte[] { 29, 86, 0 });
    }
}
