package com.mobfirst.printidthermalprintergadget.listeners;

public interface ConnectionResult {

    void success();
    void error(String error);
}
