package com.mobfirst.printidthermalprintergadget.listeners;

import android.hardware.usb.UsbDevice;

public interface PermissionResult {

    void granted(UsbDevice device);
    void denied(UsbDevice device);
    void error(String error);
}
