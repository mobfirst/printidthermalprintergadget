package com.mobfirst.printidthermalprintergadget.usb;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;

import com.mobfirst.printidthermalprintergadget.listeners.ConnectionResult;
import com.mobfirst.printidthermalprintergadget.listeners.PermissionResult;

import java.util.HashMap;
import java.util.Iterator;

public class UsbManager {

    private final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private Context context;

    private android.hardware.usb.UsbManager usbManager;
    private UsbDevice usbDevice;
    private UsbInterface usbInterface;
    private UsbEndpoint usbEndpoint;
    private UsbDeviceConnection usbConnection;

    public UsbManager(final Context context, final int vendorId, final int productId) throws Exception {

        this.context = context;

        this.usbManager = (android.hardware.usb.UsbManager) context.getSystemService(Context.USB_SERVICE);
        this.usbDevice = null;

        if(this.usbManager != null) {

            HashMap<String, UsbDevice> devices = this.usbManager.getDeviceList();
            Iterator<UsbDevice> iterator = devices.values().iterator();

            while(iterator.hasNext()) {

                UsbDevice device = iterator.next();
                if(device.getVendorId() == vendorId && device.getProductId() == productId) {
                    this.usbDevice = device;
                }
            }

            if(this.usbDevice == null) throw new Exception("device not found");
        }
        else throw new Exception("device not found");
    }

    public void connect(final int interfaceIndex, final int endpointIndex, final ConnectionResult result) {

        prepare(new PermissionResult() {
            @Override
            public void granted(UsbDevice device) {

                if(device != null) {

                    if (device.getInterfaceCount() > interfaceIndex) {
                        usbInterface = device.getInterface(interfaceIndex);

                        if (usbInterface.getEndpointCount() > endpointIndex) {
                            usbEndpoint = usbInterface.getEndpoint(endpointIndex);

                            usbConnection = usbManager.openDevice(device);
                            if (usbConnection != null) {

                                usbConnection.claimInterface(usbInterface, true);
                                result.success();
                            }
                            else result.error("failed to connect");
                        }
                        else result.error("error getting endpoint");
                    }
                    else result.error("error getting interface");
                }
                else result.error("error getting device");

            }

            @Override
            public void denied(UsbDevice device) {
                result.error("permission denied");
            }

            @Override
            public void error(String error) {
                result.error(error);
            }
        });
    }

    public void sendData(byte[] data) {
        if(data != null) this.usbConnection.bulkTransfer(this.usbEndpoint, data, data.length, 500);
    }

    private void prepare(final PermissionResult result) {

        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        PendingIntent permissionIntent = PendingIntent.getBroadcast(this.context, 0,
                new Intent(ACTION_USB_PERMISSION), 0);

        BroadcastReceiver usbReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent != null) {

                    String action = intent.getAction();
                    if(action != null) {

                        if(action.equals(ACTION_USB_PERMISSION)) {
                            synchronized(this) {

                                UsbDevice device = intent.getParcelableExtra(android.hardware.usb.UsbManager.EXTRA_DEVICE);
                                if(intent.getBooleanExtra(android.hardware.usb.UsbManager.EXTRA_PERMISSION_GRANTED, false)) {

                                    if(device != null) result.granted(device);
                                    else result.error("error getting device");
                                }
                                else result.denied(device);
                            }
                        }
                    }
                }
            }
        };

        this.context.registerReceiver(usbReceiver, filter);
        this.usbManager.requestPermission(usbDevice, permissionIntent);
    }
}
